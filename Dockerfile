FROM tomcat:8.5.45-jdk11
COPY web/target/time-tracker-web-0.3.1.war /usr/local/tomcat/webapps/
CMD ["catalina.sh", "run"]
